package initial

import (
	"sync"

	"gitlab.com/chunlee1991/goredmergin/exchange"
	"gitlab.com/chunlee1991/goredmergin/exchange/abcc"
	"gitlab.com/chunlee1991/goredmergin/exchange/bcex"
	"gitlab.com/chunlee1991/goredmergin/exchange/bgogo"
	"gitlab.com/chunlee1991/goredmergin/exchange/bibox"
	"gitlab.com/chunlee1991/goredmergin/exchange/bigone"
	"gitlab.com/chunlee1991/goredmergin/exchange/biki"
	"gitlab.com/chunlee1991/goredmergin/exchange/binance"
	"gitlab.com/chunlee1991/goredmergin/exchange/binancedex"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitbay"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitfinex"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitforex"
	"gitlab.com/chunlee1991/goredmergin/exchange/bithumb"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitmart"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitmax"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitmex"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitrue"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitstamp"
	"gitlab.com/chunlee1991/goredmergin/exchange/bittrex"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitz"
	"gitlab.com/chunlee1991/goredmergin/exchange/bkex"
	"gitlab.com/chunlee1991/goredmergin/exchange/blocktrade"
	"gitlab.com/chunlee1991/goredmergin/exchange/bw"
	"gitlab.com/chunlee1991/goredmergin/exchange/bybit"
	"gitlab.com/chunlee1991/goredmergin/exchange/coinbene"
	"gitlab.com/chunlee1991/goredmergin/exchange/coindeal"
	"gitlab.com/chunlee1991/goredmergin/exchange/coineal"
	"gitlab.com/chunlee1991/goredmergin/exchange/coinex"
	"gitlab.com/chunlee1991/goredmergin/exchange/cointiger"
	"gitlab.com/chunlee1991/goredmergin/exchange/dcoin"
	"gitlab.com/chunlee1991/goredmergin/exchange/deribit"
	"gitlab.com/chunlee1991/goredmergin/exchange/digifinex"
	"gitlab.com/chunlee1991/goredmergin/exchange/dragonex"
	"gitlab.com/chunlee1991/goredmergin/exchange/gateio"
	"gitlab.com/chunlee1991/goredmergin/exchange/goko"
	"gitlab.com/chunlee1991/goredmergin/exchange/hibitex"
	"gitlab.com/chunlee1991/goredmergin/exchange/hitbtc"
	"gitlab.com/chunlee1991/goredmergin/exchange/huobi"
	"gitlab.com/chunlee1991/goredmergin/exchange/huobidm"
	"gitlab.com/chunlee1991/goredmergin/exchange/huobiotc"
	"gitlab.com/chunlee1991/goredmergin/exchange/ibankdigital"
	"gitlab.com/chunlee1991/goredmergin/exchange/idex"
	"gitlab.com/chunlee1991/goredmergin/exchange/kraken"
	"gitlab.com/chunlee1991/goredmergin/exchange/kucoin"
	"gitlab.com/chunlee1991/goredmergin/exchange/latoken"
	"gitlab.com/chunlee1991/goredmergin/exchange/lbank"
	"gitlab.com/chunlee1991/goredmergin/exchange/liquid"
	"gitlab.com/chunlee1991/goredmergin/exchange/mxc"
	"gitlab.com/chunlee1991/goredmergin/exchange/newcapital"
	"gitlab.com/chunlee1991/goredmergin/exchange/okex"
	"gitlab.com/chunlee1991/goredmergin/exchange/okexdm"
	"gitlab.com/chunlee1991/goredmergin/exchange/otcbtc"
	"gitlab.com/chunlee1991/goredmergin/exchange/poloniex"
	"gitlab.com/chunlee1991/goredmergin/exchange/stex"
	"gitlab.com/chunlee1991/goredmergin/exchange/switcheo"
	"gitlab.com/chunlee1991/goredmergin/exchange/tokok"
	"gitlab.com/chunlee1991/goredmergin/exchange/tradeogre"
	"gitlab.com/chunlee1991/goredmergin/exchange/tradesatoshi"
	"gitlab.com/chunlee1991/goredmergin/exchange/virgocx"
	"gitlab.com/chunlee1991/goredmergin/exchange/zebitex"
)

var instance *InitManager
var once sync.Once

type InitManager struct {
	exMan *exchange.ExchangeManager
}

func CreateInitManager() *InitManager {
	once.Do(func() {
		instance = &InitManager{
			exMan: exchange.CreateExchangeManager(),
		}
	})
	return instance
}

func (e *InitManager) Init(config *exchange.Config) exchange.Exchange {
	switch config.ExName {
	case exchange.BINANCE:
		ex := binance.CreateBinance(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BITTREX:
		ex := bittrex.CreateBittrex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.COINEX:
		ex := coinex.CreateCoinex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.STEX:
		ex := stex.CreateStex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BITMEX:
		ex := bitmex.CreateBitmex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.KUCOIN:
		ex := kucoin.CreateKucoin(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.HUOBIOTC:
		ex := huobiotc.CreateHuobiOTC(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BITMAX:
		ex := bitmax.CreateBitmax(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BITSTAMP:
		ex := bitstamp.CreateBitstamp(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.OTCBTC:
		ex := otcbtc.CreateOtcbtc(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.HUOBI:
		ex := huobi.CreateHuobi(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BIBOX:
		ex := bibox.CreateBibox(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.OKEX:
		ex := okex.CreateOkex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BITZ:
		ex := bitz.CreateBitz(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.HITBTC:
		ex := hitbtc.CreateHitbtc(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.DRAGONEX:
		ex := dragonex.CreateDragonex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BIGONE:
		ex := bigone.CreateBigone(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BITFINEX:
		ex := bitfinex.CreateBitfinex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.GATEIO:
		ex := gateio.CreateGateio(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.IDEX:
		ex := idex.CreateIdex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.LIQUID:
		ex := liquid.CreateLiquid(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BITFOREX:
		ex := bitforex.CreateBitforex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.TOKOK:
		ex := tokok.CreateTokok(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.MXC:
		ex := mxc.CreateMxc(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BITRUE:
		ex := bitrue.CreateBitrue(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.TRADESATOSHI:
		ex := tradesatoshi.CreateTradeSatoshi(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.KRAKEN:
		ex := kraken.CreateKraken(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.POLONIEX:
		ex := poloniex.CreatePoloniex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.COINEAL:
		ex := coineal.CreateCoineal(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.TRADEOGRE:
		ex := tradeogre.CreateTradeogre(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.COINBENE:
		ex := coinbene.CreateCoinbene(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.IBANKDIGITAL:
		ex := ibankdigital.CreateIbankdigital(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.LBANK:
		ex := lbank.CreateLbank(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BINANCEDEX:
		ex := binancedex.CreateBinanceDex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BITMART:
		ex := bitmart.CreateBitmart(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BIKI:
		ex := biki.CreateBiki(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.DCOIN:
		ex := dcoin.CreateDcoin(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.COINTIGER:
		ex := cointiger.CreateCointiger(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.HUOBIDM:
		ex := huobidm.CreateHuobidm(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BW:
		ex := bw.CreateBw(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BITBAY:
		ex := bitbay.CreateBitbay(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.DERIBIT:
		ex := deribit.CreateDeribit(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.OKEXDM:
		ex := okexdm.CreateOkexdm(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.GOKO:
		ex := goko.CreateGoko(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BCEX:
		ex := bcex.CreateBcex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.DIGIFINEX:
		ex := digifinex.CreateDigifinex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.LATOKEN:
		ex := latoken.CreateLatoken(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.VIRGOCX:
		ex := virgocx.CreateVirgocx(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.ABCC:
		ex := abcc.CreateAbcc(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BYBIT:
		ex := bybit.CreateBybit(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.ZEBITEX:
		ex := zebitex.CreateZebitex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BITHUMB:
		ex := bithumb.CreateBithumb(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.SWITCHEO:
		ex := switcheo.CreateSwitcheo(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BLOCKTRADE:
		ex := blocktrade.CreateBlocktrade(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BKEX:
		ex := bkex.CreateBkex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.NEWCAPITAL:
		ex := newcapital.CreateNewcapital(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.COINDEAL:
		ex := coindeal.CreateCoindeal(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.HIBITEX:
		ex := hibitex.CreateHibitex(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	case exchange.BGOGO:
		ex := bgogo.CreateBgogo(config)
		if ex != nil {
			e.exMan.Add(ex)
		}
		return ex

	}
	return nil
}
