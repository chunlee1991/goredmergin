package main

// Copyright (c) 2015-2019 Bitontop Technologies Inc.
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

import (
	"log"
	"os"
	"time"

	"gitlab.com/chunlee1991/goredmergin/coin"
	"gitlab.com/chunlee1991/goredmergin/exchange"
	"gitlab.com/chunlee1991/goredmergin/exchange/abcc"
	"gitlab.com/chunlee1991/goredmergin/exchange/bcex"
	"gitlab.com/chunlee1991/goredmergin/exchange/bgogo"
	"gitlab.com/chunlee1991/goredmergin/exchange/bibox"
	"gitlab.com/chunlee1991/goredmergin/exchange/bigone"
	"gitlab.com/chunlee1991/goredmergin/exchange/biki"
	"gitlab.com/chunlee1991/goredmergin/exchange/binance"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitbay"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitforex"
	"gitlab.com/chunlee1991/goredmergin/exchange/bithumb"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitmart"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitmax"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitrue"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitstamp"
	"gitlab.com/chunlee1991/goredmergin/exchange/bittrex"
	"gitlab.com/chunlee1991/goredmergin/exchange/bitz"
	"gitlab.com/chunlee1991/goredmergin/exchange/bkex"
	"gitlab.com/chunlee1991/goredmergin/exchange/blocktrade"
	"gitlab.com/chunlee1991/goredmergin/exchange/bw"
	"gitlab.com/chunlee1991/goredmergin/exchange/bybit"
	"gitlab.com/chunlee1991/goredmergin/exchange/coinbene"
	"gitlab.com/chunlee1991/goredmergin/exchange/coindeal"
	"gitlab.com/chunlee1991/goredmergin/exchange/coineal"
	"gitlab.com/chunlee1991/goredmergin/exchange/coinex"
	"gitlab.com/chunlee1991/goredmergin/exchange/cointiger"
	"gitlab.com/chunlee1991/goredmergin/exchange/dcoin"
	"gitlab.com/chunlee1991/goredmergin/exchange/deribit"
	"gitlab.com/chunlee1991/goredmergin/exchange/digifinex"
	"gitlab.com/chunlee1991/goredmergin/exchange/dragonex"
	"gitlab.com/chunlee1991/goredmergin/exchange/gateio"
	"gitlab.com/chunlee1991/goredmergin/exchange/goko"
	"gitlab.com/chunlee1991/goredmergin/exchange/hibitex"
	"gitlab.com/chunlee1991/goredmergin/exchange/hitbtc"
	"gitlab.com/chunlee1991/goredmergin/exchange/huobi"
	"gitlab.com/chunlee1991/goredmergin/exchange/huobidm"
	"gitlab.com/chunlee1991/goredmergin/exchange/ibankdigital"
	"gitlab.com/chunlee1991/goredmergin/exchange/kraken"
	"gitlab.com/chunlee1991/goredmergin/exchange/kucoin"
	"gitlab.com/chunlee1991/goredmergin/exchange/latoken"
	"gitlab.com/chunlee1991/goredmergin/exchange/lbank"
	"gitlab.com/chunlee1991/goredmergin/exchange/liquid"
	"gitlab.com/chunlee1991/goredmergin/exchange/mxc"
	"gitlab.com/chunlee1991/goredmergin/exchange/newcapital"
	"gitlab.com/chunlee1991/goredmergin/exchange/okex"
	"gitlab.com/chunlee1991/goredmergin/exchange/okexdm"
	"gitlab.com/chunlee1991/goredmergin/exchange/otcbtc"
	"gitlab.com/chunlee1991/goredmergin/exchange/poloniex"
	"gitlab.com/chunlee1991/goredmergin/exchange/stex"
	"gitlab.com/chunlee1991/goredmergin/exchange/switcheo"
	"gitlab.com/chunlee1991/goredmergin/exchange/tokok"
	"gitlab.com/chunlee1991/goredmergin/exchange/tradeogre"
	"gitlab.com/chunlee1991/goredmergin/exchange/tradesatoshi"
	"gitlab.com/chunlee1991/goredmergin/exchange/virgocx"
	"gitlab.com/chunlee1991/goredmergin/exchange/zebitex"
	"gitlab.com/chunlee1991/goredmergin/pair"
	"gitlab.com/chunlee1991/goredmergin/test/conf"
	"gitlab.com/chunlee1991/goredmergin/utils"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Llongfile)
	exMan := exchange.CreateExchangeManager()

	if len(os.Args) > 1 {
		switch os.Args[1] {
		/* case "export":
		Init(exchange.EXCHANGE_API, "")
		utils.ConvertBaseDataToJson("./data")
		for _, ex := range exMan.GetExchanges() {
			utils.ConvertExchangeDataToJson("./data", ex)
		}
		break */
		case "json":
			Init(exchange.JSON_FILE, "./data")
			for _, ex := range exMan.GetExchanges() {
				for _, coin := range ex.GetCoins() {
					log.Printf("%s Coin %+v", ex.GetName(), coin)
				}
				for _, pair := range ex.GetPairs() {
					log.Printf("%s Pair %+v", ex.GetName(), pair)
				}
			}
			break
		case "renew":
			Init(exchange.JSON_FILE, "./data")
			updateConfig := &exchange.Update{
				ExNames: exMan.GetSupportExchanges(),
				Method:  exchange.TIME_TIGGER,
				Time:    10 * time.Second,
			}
			exMan.UpdateExData(updateConfig)
			break
		case "test":
			base := coin.Coin{
				Code: "BTC",
			}
			target := coin.Coin{
				Code: "ETH",
			}
			pair := pair.Pair{
				Base:   &base,
				Target: &target,
			}
			log.Println(pair)

			// okex.Socket(&pair)
			// stex.Socket()
			// bitfinex.Socket()
		}
	}
}

func Init(source exchange.DataSource, sourceURI string) {
	coin.Init()
	pair.Init()
	if source == exchange.JSON_FILE {
		utils.GetCommonDataFromJSON(sourceURI)
	}
	config := &exchange.Config{}
	config.Source = source
	config.SourceURI = sourceURI

	InitBinance(config)
	InitBittrex(config)
	InitCoinex(config)
	InitStex(config)
	InitKucoin(config)
	InitBitmax(config)
	InitBitstamp(config)
	InitOtcbtc(config)
	InitHuobi(config)
	InitBibox(config)
	InitOkex(config)
	InitBitz(config)
	InitHitbtc(config)
	InitDragonex(config)
	InitBigone(config)
	InitGateio(config)
	InitLiquid(config)
	InitBitforex(config)
	InitTokok(config)
	InitMxc(config)
	InitBitrue(config)
	InitTradeSatoshi(config)
	InitKraken(config)
	InitPoloniex(config)
	InitCoineal(config)
	InitTradeogre(config)
	InitCoinbene(config)
	InitIbankdigital(config)
	InitLbank(config)
	InitBitmart(config)
	InitDcoin(config)
	InitBiki(config)
	InitCointiger(config)
	InitHuobidm(config)
	InitBw(config)
	InitBitbay(config)
	InitDeribit(config)
	InitOkexdm(config)
	InitGoko(config)
	InitBcex(config)
	InitDigifinex(config)
	InitLatoken(config)
	InitVirgocx(config)
	InitAbcc(config)
	InitBybit(config)
	InitZebitex(config)
	InitBithumb(config)
	InitSwitcheo(config)
	InitBlocktrade(config)
	InitBkex(config)
	InitNewcapital(config)
	InitCoindeal(config)
	InitHibitex(config)
	InitBgogo(config)
}

func InitBinance(config *exchange.Config) {
	conf.Exchange(exchange.BINANCE, config)
	ex := binance.CreateBinance(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBittrex(config *exchange.Config) {
	conf.Exchange(exchange.BITTREX, config)
	ex := bittrex.CreateBittrex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitCoinex(config *exchange.Config) {
	conf.Exchange(exchange.COINEX, config)
	ex := coinex.CreateCoinex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitStex(config *exchange.Config) {
	conf.Exchange(exchange.STEX, config)
	ex := stex.CreateStex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitKucoin(config *exchange.Config) {
	conf.Exchange(exchange.KUCOIN, config)
	ex := kucoin.CreateKucoin(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBitmax(config *exchange.Config) {
	conf.Exchange(exchange.BITMAX, config)
	ex := bitmax.CreateBitmax(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBitstamp(config *exchange.Config) {
	conf.Exchange(exchange.BITSTAMP, config)
	ex := bitstamp.CreateBitstamp(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitOtcbtc(config *exchange.Config) {
	conf.Exchange(exchange.OTCBTC, config)
	ex := otcbtc.CreateOtcbtc(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitHuobi(config *exchange.Config) {
	conf.Exchange(exchange.HUOBI, config)
	ex := huobi.CreateHuobi(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBibox(config *exchange.Config) {
	conf.Exchange(exchange.BIBOX, config)
	ex := bibox.CreateBibox(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitOkex(config *exchange.Config) {
	conf.Exchange(exchange.OKEX, config)
	ex := okex.CreateOkex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBitz(config *exchange.Config) {
	conf.Exchange(exchange.BITZ, config)
	ex := bitz.CreateBitz(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitHitbtc(config *exchange.Config) {
	conf.Exchange(exchange.HITBTC, config)
	ex := hitbtc.CreateHitbtc(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitDragonex(config *exchange.Config) {
	conf.Exchange(exchange.DRAGONEX, config)
	ex := dragonex.CreateDragonex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBigone(config *exchange.Config) {
	conf.Exchange(exchange.BIGONE, config)
	ex := bigone.CreateBigone(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitGateio(config *exchange.Config) {
	conf.Exchange(exchange.GATEIO, config)
	ex := gateio.CreateGateio(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitLiquid(config *exchange.Config) {
	conf.Exchange(exchange.LIQUID, config)
	ex := liquid.CreateLiquid(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBitforex(config *exchange.Config) {
	conf.Exchange(exchange.BITFOREX, config)
	ex := bitforex.CreateBitforex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitTokok(config *exchange.Config) {
	conf.Exchange(exchange.TOKOK, config)
	ex := tokok.CreateTokok(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitMxc(config *exchange.Config) {
	conf.Exchange(exchange.MXC, config)
	ex := mxc.CreateMxc(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBitrue(config *exchange.Config) {
	conf.Exchange(exchange.BITRUE, config)
	ex := bitrue.CreateBitrue(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitTradeSatoshi(config *exchange.Config) {
	conf.Exchange(exchange.TRADESATOSHI, config)
	ex := tradesatoshi.CreateTradeSatoshi(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitKraken(config *exchange.Config) {
	conf.Exchange(exchange.KRAKEN, config)
	ex := kraken.CreateKraken(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitPoloniex(config *exchange.Config) {
	conf.Exchange(exchange.POLONIEX, config)
	ex := poloniex.CreatePoloniex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitCoineal(config *exchange.Config) {
	conf.Exchange(exchange.COINEAL, config)
	ex := coineal.CreateCoineal(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitTradeogre(config *exchange.Config) {
	conf.Exchange(exchange.TRADEOGRE, config)
	ex := tradeogre.CreateTradeogre(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitCoinbene(config *exchange.Config) {
	conf.Exchange(exchange.COINBENE, config)
	ex := coinbene.CreateCoinbene(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitIbankdigital(config *exchange.Config) {
	conf.Exchange(exchange.IBANKDIGITAL, config)
	ex := ibankdigital.CreateIbankdigital(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitLbank(config *exchange.Config) {
	conf.Exchange(exchange.LBANK, config)
	ex := lbank.CreateLbank(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBitmart(config *exchange.Config) {
	conf.Exchange(exchange.BITMART, config)
	ex := bitmart.CreateBitmart(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBiki(config *exchange.Config) {
	conf.Exchange(exchange.BIKI, config)
	ex := biki.CreateBiki(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitDcoin(config *exchange.Config) {
	conf.Exchange(exchange.DCOIN, config)
	ex := dcoin.CreateDcoin(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitCointiger(config *exchange.Config) {
	conf.Exchange(exchange.COINTIGER, config)
	ex := cointiger.CreateCointiger(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitHuobidm(config *exchange.Config) {
	conf.Exchange(exchange.HUOBIDM, config)
	ex := huobidm.CreateHuobidm(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBw(config *exchange.Config) {
	conf.Exchange(exchange.BW, config)
	ex := bw.CreateBw(config)

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBitbay(config *exchange.Config) {
	conf.Exchange(exchange.BITBAY, config)
	ex := bitbay.CreateBitbay(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitDeribit(config *exchange.Config) {
	conf.Exchange(exchange.DERIBIT, config)
	ex := deribit.CreateDeribit(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitOkexdm(config *exchange.Config) {
	conf.Exchange(exchange.OKEXDM, config)
	ex := okexdm.CreateOkexdm(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitGoko(config *exchange.Config) {
	conf.Exchange(exchange.GOKO, config)
	ex := goko.CreateGoko(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBcex(config *exchange.Config) {
	conf.Exchange(exchange.BCEX, config)
	ex := bcex.CreateBcex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitDigifinex(config *exchange.Config) {
	conf.Exchange(exchange.DIGIFINEX, config)
	ex := digifinex.CreateDigifinex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitLatoken(config *exchange.Config) {
	conf.Exchange(exchange.LATOKEN, config)
	ex := latoken.CreateLatoken(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitVirgocx(config *exchange.Config) {
	conf.Exchange(exchange.VIRGOCX, config)
	ex := virgocx.CreateVirgocx(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitAbcc(config *exchange.Config) {
	conf.Exchange(exchange.ABCC, config)
	ex := abcc.CreateAbcc(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBybit(config *exchange.Config) {
	conf.Exchange(exchange.BYBIT, config)
	ex := bybit.CreateBybit(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitZebitex(config *exchange.Config) {
	conf.Exchange(exchange.ZEBITEX, config)
	ex := zebitex.CreateZebitex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBithumb(config *exchange.Config) {
	conf.Exchange(exchange.BITHUMB, config)
	ex := bithumb.CreateBithumb(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitSwitcheo(config *exchange.Config) {
	conf.Exchange(exchange.SWITCHEO, config)
	ex := switcheo.CreateSwitcheo(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBlocktrade(config *exchange.Config) {
	conf.Exchange(exchange.BLOCKTRADE, config)
	ex := blocktrade.CreateBlocktrade(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBkex(config *exchange.Config) {
	conf.Exchange(exchange.BKEX, config)
	ex := bkex.CreateBkex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitNewcapital(config *exchange.Config) {
	conf.Exchange(exchange.NEWCAPITAL, config)
	ex := newcapital.CreateNewcapital(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitCoindeal(config *exchange.Config) {
	conf.Exchange(exchange.COINDEAL, config)
	ex := coindeal.CreateCoindeal(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitHibitex(config *exchange.Config) {
	conf.Exchange(exchange.HIBITEX, config)
	ex := hibitex.CreateHibitex(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}

func InitBgogo(config *exchange.Config) {
	conf.Exchange(exchange.BGOGO, config)
	ex := bgogo.CreateBgogo(config)
	log.Printf("Initial [ %12v ] ", ex.GetName())

	exMan := exchange.CreateExchangeManager()
	exMan.Add(ex)
}
