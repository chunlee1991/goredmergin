package test

// Copyright (c) 2015-2019 Bitontop Technologies Inc.
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

import (
	"log"
	"testing"

	"gitlab.com/chunlee1991/goredmergin/coin"
	"gitlab.com/chunlee1991/goredmergin/exchange"
	"gitlab.com/chunlee1991/goredmergin/exchange/stex"
	"gitlab.com/chunlee1991/goredmergin/pair"
	"gitlab.com/chunlee1991/goredmergin/test/conf"
)

/********************Public API********************/
func Test_Stex(t *testing.T) {
	e := InitStex()

	pair := pair.GetPairByKey("ETH|AIB")

	Test_Coins(e)
	Test_Pairs(e)
	Test_Pair(e, pair)
	Test_Orderbook(e, pair)
	Test_ConstraintFetch(e, pair)
	Test_Constraint(e, pair)

	Test_Balance(e, pair)
	// Test_Trading(e, pair, 0.00000001, 100)
	// Test_Withdraw(e, pair.Base, 1, "ADDRESS")
}

func InitStex() exchange.Exchange {
	coin.Init()
	pair.Init()
	config := &exchange.Config{}
	config.Source = exchange.EXCHANGE_API
	conf.Exchange(exchange.STEX, config)

	ex := stex.CreateStex(config)
	log.Printf("Initial [ %v ] ", ex.GetName())

	config = nil
	return ex
}
