package test

import (
	"log"
	"testing"

	"gitlab.com/chunlee1991/goredmergin/coin"
	"gitlab.com/chunlee1991/goredmergin/pair"

	"gitlab.com/chunlee1991/goredmergin/exchange"
	"gitlab.com/chunlee1991/goredmergin/exchange/huobi"
	"gitlab.com/chunlee1991/goredmergin/test/conf"
)

// Copyright (c) 2015-2019 Bitontop Technologies Inc.
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

/********************Public API********************/

func Test_Huobi(t *testing.T) {
	e := InitHuobi()

	p := pair.GetPairByKey("USDT|NEO")

	// Test_Coins(e)
	// Test_Pairs(e)
	// Test_Pair(e, p)
	// Test_Orderbook(e, p)
	// Test_ConstraintFetch(e, p)
	// Test_Constraint(e, p)

	data := &exchange.Margin{
		Action:   exchange.BALANCE,
		Pair:     p,
		Currency: p.Target,
	}

	err := e.MarginAction(data)
	log.Printf("Margin Err: %+v %+v", err, data)

	// Test_Balance(e, pair)
	// Test_Trading(e, pair, 0.00000001, 100)
	// Test_Withdraw(e, pair.Base, 1, "ADDRESS")
}

func InitHuobi() exchange.Exchange {
	coin.Init()
	pair.Init()
	config := &exchange.Config{}
	config.Source = exchange.EXCHANGE_API
	conf.Exchange(exchange.HUOBI, config)

	ex := huobi.CreateHuobi(config)
	log.Printf("Initial [ %v ] ", ex.GetName())

	config = nil
	return ex
}
