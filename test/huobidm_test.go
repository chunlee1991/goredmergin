package test

import (
	"log"
	"testing"

	"gitlab.com/chunlee1991/goredmergin/coin"
	"gitlab.com/chunlee1991/goredmergin/exchange"
	"gitlab.com/chunlee1991/goredmergin/pair"

	"gitlab.com/chunlee1991/goredmergin/exchange/huobidm"
	"gitlab.com/chunlee1991/goredmergin/test/conf"
	// "../exchange/huobidm"
	// "./conf"
)

// Copyright (c) 2015-2019 Bitontop Technologies Inc.
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

/********************Public API********************/

func Test_Huobidm(t *testing.T) {
	e := InitHuobidm()
	// All coin symbol are lowercased, e.g. "btc"
	// Need to rewrite Coin, Pair.

	//CW FOR THIS WEEK, NW FOR NEXT WEEK, CQ FOR QUARTER
	//BASE IS ALWAYS USD
	pair := pair.GetPairByKey("USD|NWTRX")
	coin := coin.GetCoin("CWBTC")

	// Test_Coins(e)
	// Test_Pairs(e)
	Test_Pair(e, pair)
	// Test_Orderbook(e, pair)
	// Test_ConstraintFetch(e, pair)
	// Test_Constraint(e, pair)

	// log.Printf("pair: %v", pair.Name)
	// Test_Balance(e, pair)
	// Test_Trading(e, pair, 0.0001, 100)
	// Test_OrderStatus(e, pair, "106837")

	// test contractBalance
	log.Println("Test contractBalance:")
	data0 := &exchange.Contract{
		Action:   exchange.CONTRACT_BALANCE,
		Currency: coin,
	}
	err := e.ContractAction(data0)
	if err != nil {
		log.Printf("%v", err)
	}
	log.Printf("contractBalance Return: %+v", data0.ContractBalanceReturn)

	// // test contract buy
	// log.Println("Test ContractBuy:")
	// data := &exchange.Contract{
	// 	Action:         exchange.CONTRACT_MARKET_BUY,
	// 	Pair:           pair,
	// 	Quantity:       100,
	// 	Leverage:       1,
	// 	Offset:         exchange.OPEN,      // huobidm only
	// 	OrderPriceType: exchange.OPTIMAL_5, // huobidm only
	// }
	// err = e.ContractAction(data)
	// if err != nil {
	// 	log.Printf("%s Limit Buy Err: %s", e.GetName(), err)
	// }
	// log.Printf("ContractBuy Return: %+v", data.ContractOrderReturn)

	// // test transfer between account
	// log.Println("Test Transfer:")
	// data2 := &exchange.Contract{
	// 	Action:    exchange.CONTRACT_TRANSFER,
	// 	Currency:  coin,
	// 	Quantity:  10,
	// 	Direction: exchange.SPOT_TO_FUTURE,
	// }
	// err = e.ContractAction(data2)
	// if err != nil {
	// 	log.Printf("%v", err)
	// }

	// // test getAddr
	// log.Println("Test Get Address:")
	// data3 := &exchange.Contract{
	// 	Action:   exchange.GET_ADDR,
	// 	Currency: coin,
	// }
	// err = e.ContractAction(data3)
	// if err != nil {
	// 	log.Printf("%v", err)
	// }
	// log.Printf("Deposit Address: %v", data3.Addr)

	// // Test_Withdraw_tag, last parameter: Txfee for this coin(no API to get this)
	// Test_Withdraw_tag(e, coin, 1, "3BMEXf5xUkhdoNkBjLwTvjR5Kgm7cQCafz", "0.005")

	// // test Contract OrderStatus
	// log.Println("Test ContractOrderStatus:")
	// order := &exchange.Order{
	// 	Pair:    pair, // huobidm only
	// 	OrderID: "106837",
	// }
	// data4 := &exchange.Contract{
	// 	Action: exchange.CONTRACT_ORDER_STATUS,
	// 	Order:  order,
	// }
	// err = e.ContractAction(data4)
	// if err != nil {
	// 	log.Printf("%v", err)
	// }
	// log.Printf("ContractOrderStatus Return: %v", data.ContractOrderReturn)
}

func InitHuobidm() exchange.Exchange {
	coin.Init()
	pair.Init()

	config := &exchange.Config{}
	config.Source = exchange.EXCHANGE_API
	conf.Exchange(exchange.HUOBIDM, config)

	ex := huobidm.CreateHuobidm(config)
	log.Printf("Initial [ %v ] ", ex.GetName())

	config = nil
	return ex
}
