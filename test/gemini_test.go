package test

import (
	"log"
	"testing"

	// "../exchange/gemini"
	// "./conf"
	"gitlab.com/chunlee1991/goredmergin/coin"
	"gitlab.com/chunlee1991/goredmergin/exchange"
	"gitlab.com/chunlee1991/goredmergin/exchange/gemini"
	"gitlab.com/chunlee1991/goredmergin/pair"
	"gitlab.com/chunlee1991/goredmergin/test/conf"
)

// Copyright (c) 2015-2019 Bitontop Technologies Inc.
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

/********************Public API********************/

func Test_Gemini(t *testing.T) {
	e := InitGemini()

	pair := pair.GetPairByKey("BTC|ETH")

	Test_Coins(e)
	Test_Pairs(e)
	Test_Pair(e, pair)
	Test_Orderbook(e, pair)
	Test_ConstraintFetch(e, pair)
	Test_Constraint(e, pair)

	Test_Balance(e, pair)
	Test_Trading(e, pair, 0.0001, 100)
	//Test_Withdraw(e, pair.Base, 1, "ADDRESS")
}

func InitGemini() exchange.Exchange {
	coin.Init()
	pair.Init()

	config := &exchange.Config{}
	config.Source = exchange.EXCHANGE_API
	// config.Source = exchange.JSON_FILE
	// config.SourceURI = "https://raw.githubusercontent.com/bitontop/gored/master/data"
	// utils.GetCommonDataFromJSON(config.SourceURI)

	conf.Exchange(exchange.GEMINI, config)

	ex := gemini.CreateGemini(config)
	log.Printf("Initial [ %v ] ", ex.GetName())

	config = nil
	return ex
}
