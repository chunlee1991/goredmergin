package bitmex

import "time"

// Copyright (c) 2015-2019 Bitontop Technologies Inc.
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

type ErrorResponse struct {
	Error struct {
		Message string `json:"message"`
		Name    string `json:"name"`
	} `json:"error"`
}

type MarginBalances struct {
	Account   int     `json:"account"`
	Currency  string  `json:"currency"`
	RiskLimit float64 `json:"riskLimit"`
	PrevState string  `json:"prevState"`
	State     string  `json:"state"`
	Action    string  `json:"action"`
	Amount    float64 `json:"amount"`
	// PendingCredit      int       `json:"pendingCredit"`
	// PendingDebit       int       `json:"pendingDebit"`
	// ConfirmedDebit     int       `json:"confirmedDebit"`
	// PrevRealisedPnl    int       `json:"prevRealisedPnl"`
	// PrevUnrealisedPnl  int       `json:"prevUnrealisedPnl"`
	// GrossComm          int       `json:"grossComm"`
	// GrossOpenCost      int       `json:"grossOpenCost"`
	// GrossOpenPremium   int       `json:"grossOpenPremium"`
	// GrossExecCost      int       `json:"grossExecCost"`
	// GrossMarkValue     int       `json:"grossMarkValue"`
	// RiskValue          int       `json:"riskValue"`
	// TaxableMargin      int       `json:"taxableMargin"`
	// InitMargin         int       `json:"initMargin"`
	// MaintMargin        int       `json:"maintMargin"`
	// SessionMargin      int       `json:"sessionMargin"`
	// TargetExcessMargin int       `json:"targetExcessMargin"`
	// VarMargin          int       `json:"varMargin"`
	// RealisedPnl        int       `json:"realisedPnl"`
	// UnrealisedPnl      int       `json:"unrealisedPnl"`
	// IndicativeTax      int       `json:"indicativeTax"`
	// UnrealisedProfit   int       `json:"unrealisedProfit"`
	// SyntheticMargin    int       `json:"syntheticMargin"`
	WalletBalance      float64   `json:"walletBalance"`
	MarginBalance      float64   `json:"marginBalance"`
	MarginBalancePcnt  float64   `json:"marginBalancePcnt"`
	MarginLeverage     float64   `json:"marginLeverage"`
	MarginUsedPcnt     float64   `json:"marginUsedPcnt"`
	ExcessMargin       float64   `json:"excessMargin"`
	ExcessMarginPcnt   float64   `json:"excessMarginPcnt"`
	AvailableMargin    float64   `json:"availableMargin"`
	WithdrawableMargin float64   `json:"withdrawableMargin"`
	Timestamp          time.Time `json:"timestamp"`
	GrossLastValue     float64   `json:"grossLastValue"`
	// Commission         int       `json:"commission"`
}

type AccountBalances struct {
	Account          int           `json:"account"`
	Currency         string        `json:"currency"`
	PrevDeposited    float64       `json:"prevDeposited"`
	PrevWithdrawn    float64       `json:"prevWithdrawn"`
	PrevTransferIn   float64       `json:"prevTransferIn"`
	PrevTransferOut  float64       `json:"prevTransferOut"`
	PrevAmount       float64       `json:"prevAmount"`
	PrevTimestamp    time.Time     `json:"prevTimestamp"`
	DeltaDeposited   float64       `json:"deltaDeposited"`
	DeltaWithdrawn   float64       `json:"deltaWithdrawn"`
	DeltaTransferIn  float64       `json:"deltaTransferIn"`
	DeltaTransferOut float64       `json:"deltaTransferOut"`
	DeltaAmount      float64       `json:"deltaAmount"`
	Deposited        float64       `json:"deposited"`
	Withdrawn        float64       `json:"withdrawn"`
	TransferIn       float64       `json:"transferIn"`
	TransferOut      float64       `json:"transferOut"`
	Amount           float64       `json:"amount"`
	PendingCredit    float64       `json:"pendingCredit"`
	PendingDebit     float64       `json:"pendingDebit"`
	ConfirmedDebit   float64       `json:"confirmedDebit"`
	Timestamp        time.Time     `json:"timestamp"`
	Addr             string        `json:"addr"`
	Script           string        `json:"script"`
	WithdrawalLock   []interface{} `json:"withdrawalLock"`
}

type PlaceOrder struct {
	OrderID               string      `json:"orderID"`
	ClOrdID               string      `json:"clOrdID"`
	ClOrdLinkID           string      `json:"clOrdLinkID"`
	Account               int         `json:"account"`
	Symbol                string      `json:"symbol"`
	Side                  string      `json:"side"`
	SimpleOrderQty        float64     `json:"simpleOrderQty"`
	OrderQty              float64     `json:"orderQty"`
	Price                 float64     `json:"price"`
	DisplayQty            float64     `json:"displayQty"`
	StopPx                interface{} `json:"stopPx"`
	PegOffsetValue        interface{} `json:"pegOffsetValue"`
	PegPriceType          string      `json:"pegPriceType"`
	Currency              string      `json:"currency"`
	SettlCurrency         string      `json:"settlCurrency"`
	OrdType               string      `json:"ordType"`
	TimeInForce           string      `json:"timeInForce"`
	ExecInst              string      `json:"execInst"`
	ContingencyType       string      `json:"contingencyType"`
	ExDestination         string      `json:"exDestination"`
	OrdStatus             string      `json:"ordStatus"`
	Triggered             string      `json:"triggered"`
	WorkingIndicator      bool        `json:"workingIndicator"`
	OrdRejReason          string      `json:"ordRejReason"`
	SimpleLeavesQty       float64     `json:"simpleLeavesQty"`
	LeavesQty             float64     `json:"leavesQty"`
	SimpleCumQty          float64     `json:"simpleCumQty"`
	CumQty                float64     `json:"cumQty"`
	AvgPx                 float64     `json:"avgPx"`
	MultiLegReportingType string      `json:"multiLegReportingType"`
	Text                  string      `json:"text"`
	TransactTime          time.Time   `json:"transactTime"`
	Timestamp             time.Time   `json:"timestamp"`
}

type SetLeverage struct {
	Account              int       `json:"account"`
	Symbol               string    `json:"symbol"`
	Currency             string    `json:"currency"`
	Underlying           string    `json:"underlying"`
	QuoteCurrency        string    `json:"quoteCurrency"`
	Commission           float64   `json:"commission"`
	InitMarginReq        float64   `json:"initMarginReq"`
	MaintMarginReq       float64   `json:"maintMarginReq"`
	RiskLimit            int64     `json:"riskLimit"`
	Leverage             int       `json:"leverage"`
	CrossMargin          bool      `json:"crossMargin"`
	DeleveragePercentile float64   `json:"deleveragePercentile"`
	RebalancedPnl        int       `json:"rebalancedPnl"`
	PrevRealisedPnl      int       `json:"prevRealisedPnl"`
	PrevUnrealisedPnl    int       `json:"prevUnrealisedPnl"`
	PrevClosePrice       float64   `json:"prevClosePrice"`
	OpeningTimestamp     time.Time `json:"openingTimestamp"`
	OpeningQty           float64   `json:"openingQty"`
	OpeningCost          float64   `json:"openingCost"`
	OpeningComm          float64   `json:"openingComm"`
	OpenOrderBuyQty      float64   `json:"openOrderBuyQty"`
	OpenOrderBuyCost     float64   `json:"openOrderBuyCost"`
	OpenOrderBuyPremium  float64   `json:"openOrderBuyPremium"`
	OpenOrderSellQty     float64   `json:"openOrderSellQty"`
	OpenOrderSellCost    float64   `json:"openOrderSellCost"`
	OpenOrderSellPremium float64   `json:"openOrderSellPremium"`
	ExecBuyQty           float64   `json:"execBuyQty"`
	ExecBuyCost          float64   `json:"execBuyCost"`
	ExecSellQty          float64   `json:"execSellQty"`
	ExecSellCost         float64   `json:"execSellCost"`
	ExecQty              float64   `json:"execQty"`
	ExecCost             float64   `json:"execCost"`
	ExecComm             float64   `json:"execComm"`
	CurrentTimestamp     time.Time `json:"currentTimestamp"`
	CurrentQty           float64   `json:"currentQty"`
	CurrentCost          float64   `json:"currentCost"`
	CurrentComm          float64   `json:"currentComm"`
	RealisedCost         float64   `json:"realisedCost"`
	UnrealisedCost       float64   `json:"unrealisedCost"`
	GrossOpenCost        float64   `json:"grossOpenCost"`
	GrossOpenPremium     float64   `json:"grossOpenPremium"`
	GrossExecCost        float64   `json:"grossExecCost"`
	IsOpen               bool      `json:"isOpen"`
	MarkPrice            float64   `json:"markPrice"`
	MarkValue            float64   `json:"markValue"`
	RiskValue            float64   `json:"riskValue"`
	HomeNotional         float64   `json:"homeNotional"`
	ForeignNotional      float64   `json:"foreignNotional"`
	PosState             string    `json:"posState"`
	PosCost              float64   `json:"posCost"`
	PosCost2             float64   `json:"posCost2"`
	PosCross             float64   `json:"posCross"`
	PosInit              float64   `json:"posInit"`
	PosComm              float64   `json:"posComm"`
	PosLoss              float64   `json:"posLoss"`
	PosMargin            float64   `json:"posMargin"`
	PosMaint             float64   `json:"posMaint"`
	PosAllowance         float64   `json:"posAllowance"`
	TaxableMargin        float64   `json:"taxableMargin"`
	InitMargin           float64   `json:"initMargin"`
	MaintMargin          float64   `json:"maintMargin"`
	SessionMargin        float64   `json:"sessionMargin"`
	TargetExcessMargin   float64   `json:"targetExcessMargin"`
	VarMargin            float64   `json:"varMargin"`
	RealisedGrossPnl     float64   `json:"realisedGrossPnl"`
	RealisedTax          float64   `json:"realisedTax"`
	RealisedPnl          float64   `json:"realisedPnl"`
	UnrealisedGrossPnl   float64   `json:"unrealisedGrossPnl"`
	LongBankrupt         float64   `json:"longBankrupt"`
	ShortBankrupt        float64   `json:"shortBankrupt"`
	TaxBase              float64   `json:"taxBase"`
	IndicativeTaxRate    float64   `json:"indicativeTaxRate"`
	IndicativeTax        float64   `json:"indicativeTax"`
	UnrealisedTax        float64   `json:"unrealisedTax"`
	UnrealisedPnl        float64   `json:"unrealisedPnl"`
	UnrealisedPnlPcnt    float64   `json:"unrealisedPnlPcnt"`
	UnrealisedRoePcnt    float64   `json:"unrealisedRoePcnt"`
	SimpleQty            float64   `json:"simpleQty"`
	SimpleCost           float64   `json:"simpleCost"`
	SimpleValue          float64   `json:"simpleValue"`
	SimplePnl            float64   `json:"simplePnl"`
	SimplePnlPcnt        float64   `json:"simplePnlPcnt"`
	AvgCostPrice         float64   `json:"avgCostPrice"`
	AvgEntryPrice        float64   `json:"avgEntryPrice"`
	BreakEvenPrice       float64   `json:"breakEvenPrice"`
	MarginCallPrice      float64   `json:"marginCallPrice"`
	LiquidationPrice     float64   `json:"liquidationPrice"`
	BankruptPrice        float64   `json:"bankruptPrice"`
	Timestamp            time.Time `json:"timestamp"`
	LastPrice            float64   `json:"lastPrice"`
	LastValue            float64   `json:"lastValue"`
}

type AccountInfo struct {
	Account          int       `json:"account"`
	Currency         string    `json:"currency"`
	PrevDeposited    float64   `json:"prevDeposited"`
	PrevWithdrawn    float64   `json:"prevWithdrawn"`
	PrevTransferIn   float64   `json:"prevTransferIn"`
	PrevTransferOut  float64   `json:"prevTransferOut"`
	PrevAmount       float64   `json:"prevAmount"`
	PrevTimestamp    time.Time `json:"prevTimestamp"`
	DeltaDeposited   float64   `json:"deltaDeposited"`
	DeltaWithdrawn   float64   `json:"deltaWithdrawn"`
	DeltaTransferIn  float64   `json:"deltaTransferIn"`
	DeltaTransferOut float64   `json:"deltaTransferOut"`
	DeltaAmount      float64   `json:"deltaAmount"`
	Deposited        float64   `json:"deposited"`
	Withdrawn        float64   `json:"withdrawn"`
	TransferIn       float64   `json:"transferIn"`
	TransferOut      float64   `json:"transferOut"`
	Amount           float64   `json:"amount"`
	PendingCredit    float64   `json:"pendingCredit"`
	PendingDebit     float64   `json:"pendingDebit"`
	ConfirmedDebit   float64   `json:"confirmedDebit"`
	Timestamp        time.Time `json:"timestamp"`
	Addr             string    `json:"addr"`
	Script           string    `json:"script"`
	WithdrawalLock   []string  `json:"withdrawalLock"`
}

type OrderBook []struct {
	Symbol string  `json:"symbol"`
	ID     int64   `json:"id"`
	Side   string  `json:"side"`
	Size   float64 `json:"size"`
	Price  float64 `json:"price"`
}

type PairsData []struct {
	Symbol                         string      `json:"symbol"`
	RootSymbol                     string      `json:"rootSymbol"`
	State                          string      `json:"state"`
	Typ                            string      `json:"typ"`
	Listing                        time.Time   `json:"listing"`
	Front                          time.Time   `json:"front"`
	Expiry                         time.Time   `json:"expiry"`
	Settle                         time.Time   `json:"settle"`
	RelistInterval                 interface{} `json:"relistInterval"`
	InverseLeg                     string      `json:"inverseLeg"`
	SellLeg                        string      `json:"sellLeg"`
	BuyLeg                         string      `json:"buyLeg"`
	OptionStrikePcnt               interface{} `json:"optionStrikePcnt"`
	OptionStrikeRound              interface{} `json:"optionStrikeRound"`
	OptionStrikePrice              interface{} `json:"optionStrikePrice"`
	OptionMultiplier               interface{} `json:"optionMultiplier"`
	PositionCurrency               string      `json:"positionCurrency"`
	Underlying                     string      `json:"underlying"`
	QuoteCurrency                  string      `json:"quoteCurrency"`
	UnderlyingSymbol               string      `json:"underlyingSymbol"`
	Reference                      string      `json:"reference"`
	ReferenceSymbol                string      `json:"referenceSymbol"`
	CalcInterval                   interface{} `json:"calcInterval"`
	PublishInterval                interface{} `json:"publishInterval"`
	PublishTime                    interface{} `json:"publishTime"`
	MaxOrderQty                    float64     `json:"maxOrderQty"`
	MaxPrice                       float64     `json:"maxPrice"`
	LotSize                        float64     `json:"lotSize"`
	TickSize                       float64     `json:"tickSize"`
	Multiplier                     int         `json:"multiplier"`
	SettlCurrency                  string      `json:"settlCurrency"`
	UnderlyingToPositionMultiplier int         `json:"underlyingToPositionMultiplier"`
	UnderlyingToSettleMultiplier   interface{} `json:"underlyingToSettleMultiplier"`
	QuoteToSettleMultiplier        int         `json:"quoteToSettleMultiplier"`
	IsQuanto                       bool        `json:"isQuanto"`
	IsInverse                      bool        `json:"isInverse"`
	InitMargin                     float64     `json:"initMargin"`
	MaintMargin                    float64     `json:"maintMargin"`
	RiskLimit                      int64       `json:"riskLimit"`
	RiskStep                       int64       `json:"riskStep"`
	Limit                          interface{} `json:"limit"`
	Capped                         bool        `json:"capped"`
	Taxed                          bool        `json:"taxed"`
	Deleverage                     bool        `json:"deleverage"`
	MakerFee                       float64     `json:"makerFee"`
	TakerFee                       float64     `json:"takerFee"`
	SettlementFee                  float64     `json:"settlementFee"`
	InsuranceFee                   int         `json:"insuranceFee"`
	FundingBaseSymbol              string      `json:"fundingBaseSymbol"`
	FundingQuoteSymbol             string      `json:"fundingQuoteSymbol"`
	FundingPremiumSymbol           string      `json:"fundingPremiumSymbol"`
	FundingTimestamp               interface{} `json:"fundingTimestamp"`
	FundingInterval                interface{} `json:"fundingInterval"`
	FundingRate                    interface{} `json:"fundingRate"`
	IndicativeFundingRate          interface{} `json:"indicativeFundingRate"`
	RebalanceTimestamp             interface{} `json:"rebalanceTimestamp"`
	RebalanceInterval              interface{} `json:"rebalanceInterval"`
	OpeningTimestamp               time.Time   `json:"openingTimestamp"`
	ClosingTimestamp               time.Time   `json:"closingTimestamp"`
	SessionInterval                time.Time   `json:"sessionInterval"`
	PrevClosePrice                 float64     `json:"prevClosePrice"`
	LimitDownPrice                 interface{} `json:"limitDownPrice"`
	LimitUpPrice                   interface{} `json:"limitUpPrice"`
	BankruptLimitDownPrice         interface{} `json:"bankruptLimitDownPrice"`
	BankruptLimitUpPrice           interface{} `json:"bankruptLimitUpPrice"`
	PrevTotalVolume                float64     `json:"prevTotalVolume"`
	TotalVolume                    float64     `json:"totalVolume"`
	Volume                         float64     `json:"volume"`
	Volume24H                      float64     `json:"volume24h"`
	PrevTotalTurnover              int64       `json:"prevTotalTurnover"`
	TotalTurnover                  int64       `json:"totalTurnover"`
	Turnover                       int64       `json:"turnover"`
	Turnover24H                    int64       `json:"turnover24h"`
	HomeNotional24H                float64     `json:"homeNotional24h"`
	ForeignNotional24H             float64     `json:"foreignNotional24h"`
	PrevPrice24H                   float64     `json:"prevPrice24h"`
	Vwap                           float64     `json:"vwap"`
	HighPrice                      float64     `json:"highPrice"`
	LowPrice                       float64     `json:"lowPrice"`
	LastPrice                      float64     `json:"lastPrice"`
	LastPriceProtected             float64     `json:"lastPriceProtected"`
	LastTickDirection              string      `json:"lastTickDirection"`
	LastChangePcnt                 float64     `json:"lastChangePcnt"`
	BidPrice                       float64     `json:"bidPrice"`
	MidPrice                       float64     `json:"midPrice"`
	AskPrice                       float64     `json:"askPrice"`
	ImpactBidPrice                 float64     `json:"impactBidPrice"`
	ImpactMidPrice                 float64     `json:"impactMidPrice"`
	ImpactAskPrice                 float64     `json:"impactAskPrice"`
	HasLiquidity                   bool        `json:"hasLiquidity"`
	OpenInterest                   float64     `json:"openInterest"`
	OpenValue                      int64       `json:"openValue"`
	FairMethod                     string      `json:"fairMethod"`
	FairBasisRate                  float64     `json:"fairBasisRate"`
	FairBasis                      float64     `json:"fairBasis"`
	FairPrice                      float64     `json:"fairPrice"`
	MarkMethod                     string      `json:"markMethod"`
	MarkPrice                      float64     `json:"markPrice"`
	IndicativeTaxRate              float64     `json:"indicativeTaxRate"`
	IndicativeSettlePrice          float64     `json:"indicativeSettlePrice"`
	OptionUnderlyingPrice          interface{} `json:"optionUnderlyingPrice"`
	SettledPrice                   interface{} `json:"settledPrice"`
	Timestamp                      time.Time   `json:"timestamp"`
}

type GetLiquidation []struct {
	OrderID   string  `json:"orderID"`
	Symbol    string  `json:"symbol"`
	Side      string  `json:"side"`
	Price     float64 `json:"price"`
	LeavesQty float64 `json:"leavesQty"`
}

type Withdraw struct {
	TransactID     string    `json:"transactID"`
	Account        int       `json:"account"`
	Currency       string    `json:"currency"`
	TransactType   string    `json:"transactType"`
	Amount         int       `json:"amount"`
	Fee            int       `json:"fee"`
	TransactStatus string    `json:"transactStatus"`
	Address        string    `json:"address"`
	Tx             string    `json:"tx"`
	Text           string    `json:"text"`
	TransactTime   time.Time `json:"transactTime"`
	Timestamp      time.Time `json:"timestamp"`
}
